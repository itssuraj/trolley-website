class CreateApprovedTrucks < ActiveRecord::Migration[5.0]
  def change
    create_table :approved_trucks do |t|
      t.string :trucks_sent
      t.belongs_to :loading, foreign_key: true

      t.timestamps
    end
  end
end
