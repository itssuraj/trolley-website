class AddUserCompanyNameToApprovedTruck < ActiveRecord::Migration[5.0]
  def change
    add_column :approved_trucks, :user_company_name, :string
  end
end
