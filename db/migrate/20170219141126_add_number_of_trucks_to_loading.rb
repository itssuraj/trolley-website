class AddNumberOfTrucksToLoading < ActiveRecord::Migration[5.0]
  def change
    add_column :loadings, :number_of_trucks, :integer
  end
end
