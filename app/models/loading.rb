class Loading < ApplicationRecord
	belongs_to :user
	has_many :approved_trucks
end
