// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require sweetalert
//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require sweet-alert-confirm
//= require jquery.easing
//= require wow
//= require turbolinks
//= require_tree .

window.onload=function () {
  var defaultBounds = new google.maps.LatLngBounds(
  new google.maps.LatLng(23.6102, 85.2799));


var input = document.getElementById('loading_from');
  var second_input = document.getElementById('loading_to');
var options = {
  bounds: defaultBounds,
  types: ['address'],
  componentRestrictions: {country: "in"}
};

autocomplete = new google.maps.places.Autocomplete(input, options);
  autocomplete = new google.maps.places.Autocomplete(second_input, options);
  
};
var sweetAlertConfirmConfig = {
   title: "Are you sure?",
  text: "You will not be able to recover this imaginary file!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Yes, delete it!",
  cancelButtonText: "No, cancel plx!",
  closeOnConfirm: false,
  closeOnCancel: false,
  animation: true
};
$(document).on('click', '.trigger', function (event) {
    event.preventDefault();
    $('#modal').iziModal('open', this);
});
