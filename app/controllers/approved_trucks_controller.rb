class ApprovedTrucksController < ApplicationController
	before_action :authenticate_user!
	def create
		@loading = Loading.find(params[:loading_id])
		@approved_truck = ApprovedTruck.create(params[:approved_truck].permit(:trucks_sent))
		@approved_truck.user_id = current_user.id
		@approved_truck.user_company_name = current_user.company_name
		@approved_truck.loading_id = @loading.id

		if @approved_truck.save
			redirect_to loading_path(@loading)
		else
			render 'new'
		end
	end

end
